#!/bin/bash

RUNNING=$(netstat -apn | grep ":80 " | grep "LISTEN" | awk '{print $7}' | cut -d / -f 1)
if [ ! -z "$RUNNING" ]; then
    echo "Port 80 is currently in use, kill responsible process before starting eegle platform"
    exit 1
fi
TESTEXISTS=$(cat /etc/hosts | grep "127\.0\.0\.1" | grep "local.eegle.io")
if [ -z "$TESTEXISTS" ]; then
echo "No local.eegle.io entry found in hosts, trying to add it via sudo"
sudo echo "127.0.0.1    local.eegle.io" >> /etc/hosts
TESTEXISTS=$(cat /etc/hosts | grep "127\.0\.0\.1" | grep "local.eegle.io")
if [ -z "$TESTEXISTS" ]; then
echo "No local.eegle.io entry , sudo failed, add '127.0.0.1   local.eegle.io' line in /etc/hosts in order to use Eegle integration platform"
exit 1
fi
fi

DOCKER_EXISTS=$(command -v docker)
if [ -z "$DOCKER_EXISTS" ]; then
echo "Docker CE must be installed to launch integration platform"
exit 1
fi

GROUP_OK=$(groups | grep "docker")
if [ -z "$GROUP_OK" ]; then
echo "current user ($USER) should be added in docker group"
exit 1
fi

DOCKER_IMGNAME=registry.gitlab.com/eegle-public/eegle-integration/eegle_platform_integration:latest
if [ -z "$EEGLE_DOCKER_LINK" ]; then
EEGLE_DOCKER_LINK="${HOME}/.eegle_integration"
echo "Using $EEGLE_DOCKER_LINK as root dir for app deployment..."
fi

INT_APPDIR=$EEGLE_DOCKER_LINK/eegle/apps
INT_APPASSETSDIR=$EEGLE_DOCKER_LINK/eegle/static/app_assets
mkdir -p $INT_APPDIR
mkdir -p $INT_APPASSETSDIR

ACCOUNTS_DEV_IP=51.145.133.4

docker run -d --add-host=accounts-dev.eegle.io:$ACCOUNTS_DEV_IP \
-v $INT_APPDIR:/home/ubuntu/local.eegle.io/eegle/apps \
-v $EEGLE_DOCKER_LINK/eegle/static/app_assets:/home/ubuntu/local.eegle.io/eegle/static/app_assets  \
-p 80:80  $DOCKER_IMGNAME
