# Eegle Integration

This project holds a repository that enables "external dev"  integration.

# How to run eegle integration platform

- Checkout this project (eegle-integration)
- go into run directory
- launch ./run_platform.sh
- open a web browser and go to http://local.eegle.io


# Requirements

 - Docker CE must be installed
 - User that launches script should have rights to run docker (be part of docker group)
 - The port 80 on host should be available
 